﻿var DeathSound : AudioSource;

function OnTriggerEnter (col : Collider) {
	GlobalLives.LivesAmount -= 1;
	DeathSound.Play();
	yield WaitForSeconds(1.3);
	Application.LoadLevel(EditorApplication.currentScene);
	if (GlobalLives.LivesAmount < 0 ) {
		Application.LoadLevel("Snow001");
	}
}