﻿var PipeSound : AudioSource;
var FadeScreen : GameObject;
var MainCam : GameObject;
var SecondCam : GameObject;
var PipeEntry : GameObject;
var MainPlayer : GameObject;

function OnTriggerEnter (col : Collider) {
	PipeSound.Play();
	FadeScreen.SetActive(true);
	(FadeScreen.GetComponent("Animator") as MonoBehaviour).enabled = true;
	yield WaitForSeconds(0.495);
	(FadeScreen.GetComponent("Animator") as MonoBehaviour).enabled = false;
	MainCam.SetActive(true);
	SecondCam.SetActive(false);
	//var position = GameObject.Find("Pipe003").transform.position;
	//Debug.Log("Position:", position);
	MainPlayer.transform.position = Vector3(-2.5, -12.5, 40);
	(PipeEntry.GetComponent("Animator") as MonoBehaviour).enabled = true;
	(FadeScreen.GetComponent("Animator") as MonoBehaviour).enabled = true;
	yield WaitForSeconds(0.495);
	(FadeScreen.GetComponent("Animator") as MonoBehaviour).enabled = false;
	yield WaitForSeconds(0.495);
	(PipeEntry.GetComponent("Animator") as MonoBehaviour).enabled = false;
	//MainCam.transform.position =  GameObject.FindGameObjectWithTag("Player").transform.position;
	FadeScreen.SetActive(false);
}