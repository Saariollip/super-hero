﻿
var PerspectiveZoomAmount: float = 3.0;
var MaximumZoom: float = 125;
var MinimumZoom: float = 3;
var OrthogrphicZoomAmount: float = 0.5;

function Update ()
{
    // -------------------Code for Zooming Out------------
    if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            if (Camera.main.fieldOfView<=MaximumZoom)
                Camera.main.fieldOfView += PerspectiveZoomAmount;
            if (Camera.main.orthographicSize<=20)
                                Camera.main.orthographicSize +=0.5;
 
        }
    // ---------------Code for Zooming In------------------------
     if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (Camera.main.fieldOfView>MinimumZoom)
                Camera.main.fieldOfView -=2;
            if (Camera.main.orthographicSize>=1)
                                Camera.main.orthographicSize -=0.5;
        }
       
    // -------Code to switch camera between Perspective and Orthographic--------
     if (Input.GetKeyUp(KeyCode.B ))
    {
        if (Camera.main.orthographic==true)
            Camera.main.orthographic=false;
        else
            Camera.main.orthographic=true;
    }
}
 
