﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotateAround : MonoBehaviour {

    public float rotateSpeed = 25;
    public float originVectorX = 60;
    public float originVectorY = 8;
    public float originVectorZ = -0.5f;
    private Quaternion originalRotation;

    void Start()
    {
        Quaternion originalRotation = transform.rotation;
    }
    void Update()
    {
        Vector3 origin = new Vector3(originVectorX, originVectorY, originVectorZ);
        transform.RotateAround(origin, Vector3.forward, rotateSpeed * Time.deltaTime);
        transform.rotation = originalRotation;
    }
}
