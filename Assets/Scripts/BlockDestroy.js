﻿var XPos : float = 0;
var YPos : float = 0;
var ZPos : float = 0;
var Waiting : float = 0.02;

function Start () {
	XPos = transform.position.x;
	YPos = transform.position.y;
	ZPos = transform.position.z;
}


function OnTriggerEnter (col : Collider) {
	if (col.gameObject.tag == "Player") {
		this.transform.position = Vector3(XPos, YPos+0.1, ZPos);
		yield WaitForSeconds(Waiting);
		this.transform.position = Vector3(XPos, YPos+0.2, ZPos);
		yield WaitForSeconds(Waiting);
		transform.GetComponent.<Collider>().isTrigger = false;
		this.transform.position = Vector3(XPos, YPos+0.3, ZPos);
		yield WaitForSeconds(Waiting);
		this.transform.position = Vector3(XPos, YPos+0.4, ZPos);
		yield WaitForSeconds(Waiting);
		this.transform.position = Vector3(XPos, YPos-0.1, ZPos);
		yield WaitForSeconds(Waiting);
		this.transform.position = Vector3(XPos, YPos-0.6, ZPos);
		yield WaitForSeconds(Waiting);
		this.transform.position = Vector3(XPos, YPos-1.6, ZPos);
		yield WaitForSeconds(Waiting);
		this.transform.position = Vector3(XPos, YPos-2.6, ZPos);
		yield WaitForSeconds(Waiting);
		this.transform.position = Vector3(XPos, YPos-4.0, ZPos);
		yield WaitForSeconds(0.25);
		transform.GetComponent.<Collider>().isTrigger = true;
		Destroy(gameObject);
	}
}
