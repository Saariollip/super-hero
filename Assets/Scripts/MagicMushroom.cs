﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagicMushroom : MonoBehaviour {

    public GameObject shroom, camera;
    private GameObject[] polys;
    private int i;

    void OnTriggerEnter(Collider col)
    {
        Destroy(shroom);
        polys = GameObject.FindGameObjectsWithTag("Poly");
  

        foreach (GameObject poly in polys)
        {
            if (poly.activeSelf == true)
            {
                poly.transform.gameObject.SetActive(false);
            }
            else
            {
                Debug.Log(poly);
                poly.transform.gameObject.SetActive(true);
            }
            //poly.transform.DetachChildren();
            for (i = 0; i < poly.transform.GetChildCount(); ++i)
            {
                poly.transform.GetChild(i).gameObject.SetActive(true);
                if ( i == (poly.transform.GetChildCount()-1) )
                {
                    poly.transform.GetChild(i).gameObject.SetActive(true);                 
                    poly.transform.GetChild(i).parent = null;
                    camera.GetComponent<High>().enabled = true;
                }
            }
        }
        /*
        for (i = 0; i < polys.length; i++)
        {
            polys[i].setActive(true);
        }
        */

    }
}
