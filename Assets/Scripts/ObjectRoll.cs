﻿using UnityEngine;

public class ObjectRoll : MonoBehaviour
{

    public float rotateSpeed = 25;
    public string direction = "forward";
    private Vector3 directionVec;

    void Start()
    {
        if (direction == "right")
        {
            directionVec = Vector3.right;
        }
        else if (direction == "left")
        {
            directionVec = Vector3.left;
        }
        else if (direction == "down")
        {
            directionVec = Vector3.down;
        }
        else if (direction == "up")
        {
            directionVec = Vector3.up;
        }
        else
        {
            directionVec = Vector3.forward;
        }

    }
    void Update()
    {
        transform.Rotate(directionVec * Time.deltaTime * rotateSpeed);
    }
}