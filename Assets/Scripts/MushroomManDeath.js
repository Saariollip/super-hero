﻿var MushroomMan : GameObject;
var sizeMinimizer : float = 0.1;

function OnTriggerEnter (col : Collider) {
	this.GetComponent("BoxCollider").enabled = false;
	MushroomMan.GetComponent("ObjectMove2D").enabled = false;
	MushroomMan.transform.localScale -= new Vector3(0, 0, sizeMinimizer);
	MushroomMan.transform.localPosition -= new Vector3(0, 0, 0.8);
	yield WaitForSeconds (0.5);
	Destroy(MushroomMan);
	//MushroomMan.transform.position = Vector3(0, -1000, 0);
}