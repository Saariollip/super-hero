﻿var ActualMushroom : GameObject;
var ThisMushroom : GameObject;


function Update () {
	transform.parent = null;	
	if (transform.position.y <= 4.2 && transform.position.y > -500) {
		transform.Translate(Vector3.up * 2.1 * Time.deltaTime, Space.World);
	}
	else {
		ThisMushroom.transform.position = Vector3(0, -1000, 0);
	}
	CloseAnim();
}

function CloseAnim () {
	yield WaitForSeconds(0.5);
	ActualMushroom.SetActive(true);
	ThisMushroom.SetActive(false);
}