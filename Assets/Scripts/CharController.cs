﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharController : MonoBehaviour {

    [System.Serializable]
    public class MoveSettings
    {
        public float rotateVel = 100;
        public float forwardVel = 12;
        public float jumpVel = 25;
        public float distToGrounded = 0.1f;
        public LayerMask ground;
    }

    [System.Serializable]
    public class PhysSettings
    {
        public float downAccel = 0.75f;
    }

    [System.Serializable]
    public class InputSettings
    {
        public float inputDelay = 0.1f;
        public string FORWARD_AXIS = "Vertical";
        public string TURN_AXIS = "Horizontal";
        public string JUMP_AXIS = "Jump";
    }

   [SerializeField] float animSpeedMultiplier = 1f;

    public MoveSettings moveSetting = new MoveSettings();
    public PhysSettings physSetting = new PhysSettings();
    public InputSettings inputSetting = new InputSettings();

    Vector3 velocity = Vector3.zero;
    Quaternion targetRotation;
    Rigidbody rBody;
    Animator animator;
    float forwardInput, turnInput, jumpInput;
    bool grouching;


    public Quaternion TargetRotation
    {
        get { return targetRotation; }
    }

    bool Grounded()
    {
        return Physics.Raycast(transform.position, Vector3.down, moveSetting.distToGrounded, moveSetting.ground);
    }
    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
        targetRotation = transform.rotation;
        if (GetComponent<Rigidbody>())
            rBody = GetComponent<Rigidbody>();
        else
            Debug.LogError("The character needs a rigidbody.");

        forwardInput = turnInput = jumpInput = 0;
	}
	
    void GetInput()
    {
        forwardInput = Input.GetAxis(inputSetting.FORWARD_AXIS); //returns value -1...1
        turnInput = Input.GetAxis(inputSetting.TURN_AXIS); //returns value -1...1
        jumpInput = Input.GetAxisRaw(inputSetting.JUMP_AXIS);
    }

	// Update is called once per frame
	void Update () {
        GetInput();
        Turn();
	}

    void FixedUpdate()
    {
        Run();
        Jump();

        rBody.velocity = transform.TransformDirection(velocity);
    }

    void UpdateAnimator(Vector3 move)
    {
        // update the animator parameters
        animator.SetFloat("Forward", moveSetting.forwardVel, 0.1f, Time.deltaTime);
        animator.SetFloat("Turn", moveSetting.rotateVel, 0.1f, Time.deltaTime);
        //animator.SetBool("Crouch", );
        animator.SetBool("OnGround", Grounded());
        if (!Grounded())
        {
            animator.SetFloat("Jump", rBody.velocity.y);
        }

        // calculate which leg is behind, so as to leave that leg trailing in the jump animation
        // (This code is reliant on the specific run cycle offset in our animations,
        // and assumes one leg passes the other at the normalized clip times of 0.0 and 0.5)
        /*
        float runCycle =
            Mathf.Repeat(
                animator.GetCurrentAnimatorStateInfo(0).normalizedTime + m_RunCycleLegOffset, 1);
        float jumpLeg = (runCycle < k_Half ? 1 : -1) * moveSetting.forwardVel;
        if (Grounded())
        {
            animator.SetFloat("JumpLeg", jumpLeg);
        }
        */
        // the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
        // which affects the movement speed because of the root motion.
        if (Grounded() && move.magnitude > 0)
        {
            animator.speed = animSpeedMultiplier;
        }
        else
        {
            // don't use that while airborne
            animator.speed = 1;
        }
    }

    void Run()
    {
        if (Mathf.Abs(forwardInput) > inputSetting.inputDelay)
        {
            //move
            velocity.z = moveSetting.forwardVel * forwardInput;
        }
        else
        {
            velocity.z = 0;
        }
    }
    void Turn()
    {
        if (Mathf.Abs(turnInput) > inputSetting.inputDelay)
        {
            targetRotation *= Quaternion.AngleAxis(moveSetting.rotateVel * turnInput * Time.deltaTime, Vector3.up);
        }
        transform.rotation = targetRotation;
    }
    void Jump()
    {
        if (jumpInput > 0 && Grounded())
        {
            velocity.y = moveSetting.jumpVel;
        }
        else if (jumpInput == 0 && Grounded())
        {
            velocity.y = 0;
        }
        else
        {
            // decrease velocity.y
            velocity.y -= physSetting.downAccel;
        }
    }

    public void OnAnimatorMove()
    {
        // we implement this function to override the default root motion.
        // this allows us to modify the positional speed before it's applied.
        if (Grounded() && Time.deltaTime > 0)
        {
            Vector3 v = (animator.deltaPosition * moveSetting.forwardVel) / Time.deltaTime;

            // we preserve the existing y part of the current velocity.
            v.y = rBody.velocity.y;
            rBody.velocity = v;
        }
    }
}
