﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class WorldInteraction : MonoBehaviour {
    NavMeshAgent playerAgent;

    private void Start()
    {
        playerAgent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
       if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
       {
            GetInteraction();
       }
    }
    void GetInteraction()
    {
        Ray interactionRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit interactionInfo;
        if (Physics.Raycast(interactionRay, out interactionInfo, Mathf.Infinity))
        {
            GameObject interactedObject = interactionInfo.collider.gameObject;
            if (interactedObject.tag == "Interacteble Object")
            {
                Debug.Log("Interacteble interacted.");
            }
            else
            {
                playerAgent.destination = interactionInfo.point;
            }
        }
    }
}
