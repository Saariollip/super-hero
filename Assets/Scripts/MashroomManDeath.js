﻿var MushroomMan : GameObject;

function OnTriggerEnter (col : Collider) {
	(this.GetComponent("BoxCollider") as MonoBehaviour).enabled = false;
	(MushroomMan.GetComponent("MushroomManMove") as MonoBehaviour).enabled = false;
	MushroomMan.transform.localScale -= new Vector3(0, 0, 0.01);
	MushroomMan.transform.localPosition -= new Vector3(0, 0, 0.005);
	yield WaitForSeconds (1);
	MushroomMan.transform.position = Vector3(0, -1000, 0);
}